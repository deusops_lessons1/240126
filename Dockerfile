FROM node:16 as builder
WORKDIR /srv/
COPY . .
RUN npm install --include=dev &&\
        npm run build
FROM node:16-slim
WORKDIR /srv/
COPY --from=builder /srv /srv
CMD ["npm","start"]