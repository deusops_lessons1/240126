[app]
%{ for ip in app_hosts ~}
${ip}
%{ endfor ~}

[haproxy]
%{ for ip in haproxy_hosts ~}
${ip}
%{ endfor ~}

[all:vars]
ansible_user=ubuntu