terraform {
 backend "s3" {
   bucket         = "terr-backend"
   key            = "state/terraform.tfstate"
   region         = "us-east-1"
   encrypt        = true
   kms_key_id     = "alias/terraform-bucket-key"
   dynamodb_table = "terraform-state"
   access_key = "AKIAZQ3DOC7BNMSDMU6G"
   secret_key = "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"

 }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "AKIAZQ3DOC7BNMSDMU6G"
  secret_key = "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"
}

resource "aws_instance" "app" {
    count = 3
    ami = data.aws_ami.myami.id
    vpc_security_group_ids = [aws_security_group.main.id]
    instance_type = "t2.micro"
    
    tags = {
        Name = "myapp ${count.index}"
    }
    key_name = aws_key_pair.deployer.key_name
    user_data = <<EOF
#!/bin/bash
echo "Copying the SSH Key to the server"
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8Oz9ZtRnDDoh8QbsT4ob1Q+WrMTx+KS/QHk+/BkCdBjp4SL1CcgRVEwIN7Lso0r8Stqxz6j0yUgLVxgLhZ3pTli9O0YTXt12UEXIn+RuyRbUhNVQcolT1bk/88vrRoWkjk7m9BVPVNxn4l0bXS/MFPXGzppC+Ksx0+fTe0de7xM+pyVAj5QQc+7adHCQA9WRUaemNYz8HxqcE0zyAPni774ZuEODWphY8EoqFBvj5u+dMW3KiRyKqTmmU39+kVHCUDdgY96xDb3kAyodgTfk7Qb0YZAtGHa2ueJTXdPfb2DDUVn5AaBL62Fu+zaIvldbTAMgCcgBruHQ4uYXEP1fh pavel.shiakhmetov@gcore.lu" >> /home/ubuntu/.ssh/authorized_keys
EOF
}

resource "aws_instance" "haproxy" {
    ami = data.aws_ami.myami.id
    vpc_security_group_ids = [aws_security_group.main.id]
    instance_type = "t2.micro"
    
    tags = {
        Name = "haproxy"
    }
    key_name = aws_key_pair.deployer.key_name
    user_data = <<EOF
#!/bin/bash
echo "Copying the SSH Key to the server"
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8Oz9ZtRnDDoh8QbsT4ob1Q+WrMTx+KS/QHk+/BkCdBjp4SL1CcgRVEwIN7Lso0r8Stqxz6j0yUgLVxgLhZ3pTli9O0YTXt12UEXIn+RuyRbUhNVQcolT1bk/88vrRoWkjk7m9BVPVNxn4l0bXS/MFPXGzppC+Ksx0+fTe0de7xM+pyVAj5QQc+7adHCQA9WRUaemNYz8HxqcE0zyAPni774ZuEODWphY8EoqFBvj5u+dMW3KiRyKqTmmU39+kVHCUDdgY96xDb3kAyodgTfk7Qb0YZAtGHa2ueJTXdPfb2DDUVn5AaBL62Fu+zaIvldbTAMgCcgBruHQ4uYXEP1fh pavel.shiakhmetov@gcore.lu" >> /home/ubuntu/.ssh/authorized_keys
EOF
}

data "aws_ami" "myami" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
    }
}

resource "aws_security_group" "main" {
  egress = [
    {
      cidr_blocks      = ["0.0.0.0/0", ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    }
  ]

  ingress = [
    {
      cidr_blocks      = ["0.0.0.0/0", ]
      description      = "ssh"
      from_port        = 22
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "tcp"
      security_groups  = []
      self             = false
      to_port          = 22
    },
    {
      cidr_blocks      = ["0.0.0.0/0", ]
      description      = "http"
      from_port        = 80
      ipv6_cidr_blocks = []
        prefix_list_ids  = []
        protocol         = "tcp"
        security_groups  = []
        self             = false
        to_port          = 80
    },
    {
      cidr_blocks      = ["0.0.0.0/0", ]
        description      = "https"
        from_port        = 443
        ipv6_cidr_blocks = []
        prefix_list_ids  = []
        protocol         = "tcp"
        security_groups  = []
        self             = false
        to_port          = 443

    }
  ]
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCkmvXvHyCGoAqEA6zDJo1PZXi3qm9r6b5F0wEZWlVjKT5BBZYPg+xU++SEpSPO6qAcFtgqJx7SmTNYWZ8W2YmdTROWcf7ieFhKAm4PkdUE4PxaRy+RammN2c80s82qxk1DUoibXrq3CDQADH/PBiJFuXVNBI6U110JVLDdPxUCht2idfwnKxaJiGvlOo3WGOB+MEg8pndsYoekibp7Ys089tHYjhsz8SfHoLdv5p8emHKzmaHUDgOV7FUQF3CsoXhJQymzBia4MRgR/FfdwubGiaA4fYqtoSd1gVlpSzqAU3dsZPjIqKjAYrXca+gg8Qy/Kwj7nVr4CxAi5h4++QuhzdsNXZvT7GFZeI/OgsrJVZMqr2/f/NnpStTFVThcolmIf7c8q8H5Iupo0fNRttVuDuxMD+R6TqUUBdUrxUa+MtuGY/FRQOnwBAV8jcO/3BbMSZzOHhpkzSTE+rV+2Gd5UP2dHNTYS/fCaeWdy9i0ojZjTqt1hdIH33/F1o+7Vrc= root@ip-172-31-30-102"
  
}


resource "local_file" "inventory" {
    content = templatefile("inventory_hosts.tpl",
    {
        app_hosts = aws_instance.app.*.public_ip,
        haproxy_hosts = aws_instance.haproxy.*.public_ip
    })
    filename = "./ansible/inventory/dev/app{$CI_COMMIT_SHA}.ini"
}